import React from 'react';
import Table from './Table.js'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      duration1: "week",
      looparr: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handle1 = this.handle1.bind(this);

  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  handle1(event) {
    this.setState({
      duration1: event.target.value
    });
  }

  handleSubmit(event) {
    var arr = [];
    var inputsymbol = this.state.value;
    var splitData = inputsymbol.split(',');
    for (var i = 0; i < splitData.length; i++) {
      arr[i] = (splitData[i]).toUpperCase().trim();
    }
    this.setState({
      looparr: arr
    });
  }

  render() {
    var loopArr = this.state.looparr;
    var select = this.state.duration1;
    return ( <
      div align = "center" >
      <
      label >
      Stock Name:
      <
      input type = "text"
      value = {
        this.state.value
      }
      onChange = {
        this.handleChange
      }
      /> <
      /label><br / > < br / >
      <
      label >
      Duration:
      <
      select value = {
        this.state.duration1
      }
      onChange = {
        this.handle1
      } >
      <
      option value = "week" > one week < /option> <
      option value = "month" > one month < /option> <
      option value = "year" > one year < /option> <
      /select> <
      /label> <
      br / > < br / >
      <
      button onClick = {
        this.handleSubmit
      } > Get Info < /button><br / > < br / > {
        loopArr.map(function(index) {
          console.log('Inside the Function');
          return <Table key = {
            index
          }
          input = {
            index
          }
          selected = {
            select
          }
          />
        })
      } <
      /div>
    );
  }
}


export default App;
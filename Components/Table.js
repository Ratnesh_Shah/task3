import React from 'react';
// Component For Table
class Table extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '',
      duration1: '',
      data1: [],
      max: 0,
      min: 0,
      average2: [],
      total: ''
    };
  }

  componentWillMount(newProps) {
    this.state.value = this.props.input;
    this.state.duration1 = this.props.selected;
    this.getDailyList();
  }


  getDailyList() {
    fetch('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + this.state.value + '&outputsize=full&apikey=7U248YIVMXZYVEBI')
      .then(results => results.json()).then(data => {
        this.setState({
          data1: data["Time Series (Daily)"]
        });
        this.setState({
          max: this.findMax(),
          min: this.findMin(),
          average2: this.average1()
        });
      });

  }
  findMax() {
    var datapassed = this.state.data1;
    var durationis = this.state.duration1;
    var max = 0,
      count1 = 0;
    var x = "";
    if (durationis == 'month') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] > max && count1 < 30) {
          max = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return max.toFixed(2);
    } else if (durationis == 'week') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] > max && count1 < 5) {
          max = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return max.toFixed(2);
    } else if (durationis == 'year') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] > max && count1 < 365) {
          max = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return max.toFixed(2);
    }
  }

  findMin() {
    var datapassed = this.state.data1;
    var durationis = this.state.duration1;
    var min = 10000000,
      count1 = 0;
    var x = "";
    if (durationis == 'month') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] < min && count1 < 30) {
          min = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return min.toFixed(2);
    } else if (durationis == 'week') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] < min && count1 < 5) {
          min = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return min.toFixed(2);
    } else if (durationis == 'year') {
      for (x in datapassed) {
        if (datapassed[x]["2. high"] < min && count1 < 365) {
          min = Number(datapassed[x]["2. high"]);

        }
        count1 = count1 + 1;
      }
      return min.toFixed(2);
    }
  }

  average1() {
    var arr = [0, 0, 0, 0, 0, 0, 0];
    var index_cnt = [0, 0, 0, 0, 0, 0, 0];
    var avg = [0, 0, 0, 0, 0, 0, 0];
    var datapassed = this.state.data1;
    var durationis = this.state.duration1;
    var count = 0,
      sum = 0;
    var x = "";
    var i = "";
    if (durationis == 'week') {
      for (x in datapassed) {
        if (count < 7) {
          var d = new Date(x);
          var d1 = d.getDay();
          arr[d1] = arr[d1] + Number(datapassed[x]["4. close"]);
          console.log(Number(datapassed[x]["4. close"]));
          index_cnt[d1] = index_cnt[d1] + 1;
          count = count + 1;
        }
      }
    } else if (durationis == 'month') {
      for (x in datapassed) {
        if (count < 30) {
          var d = new Date(x);
          var d1 = d.getDay();
          arr[d1] = arr[d1] + Number(datapassed[x]["4. close"]);
          index_cnt[d1] = index_cnt[d1] + 1;
          count = count + 1;
        }
      }
    } else if (durationis == 'year') {
      for (x in datapassed) {
        if (count < 365) {
          var d = new Date(x);
          var d1 = d.getDay();
          arr[d1] = arr[d1] + Number(datapassed[x]["4. close"]);
          index_cnt[d1] = index_cnt[d1] + 1;
          count = count + 1;
        }
      }
    }

    for (i = 0; i < 7; i++) {
      avg[i] = (Number(arr[i]) / Number(index_cnt[i])).toFixed(2);
      console.log(sum);
    }

    for (i = 1; i < 6; i++) {
      sum = sum + Number(avg[i]);
    }
    sum = (sum / 5).toFixed(2);
    this.setState({
      total: sum
    });
    return avg;
  }


  render() {
    return ( <
      div align = "center" >
      <
      h2 > {
        this.state.value
      } < /h2> <
      label > Max: {
        this.state.max
      } < /label><br / >
      <
      label > Min: {
        this.state.min
      } < /label><br / > < br / > < br / >
      <
      table border = "1" >
      <
      tbody >
      <
      tr >
      <
      th > Weekday < /th> <
      th > Average < /th> < /
      tr > <
      tr >
      <
      td > Monday < /td> <
      td > {
        this.state.average2[1]
      } < /td> < /
      tr > <
      tr >
      <
      td > Tuesday < /td> <
      td > {
        this.state.average2[2]
      } < /td> < /
      tr > <
      tr >
      <
      td > Wednesday < /td> <
      td > {
        this.state.average2[3]
      } < /td> < /
      tr > <
      tr >
      <
      td > Thursday < /td> <
      td > {
        this.state.average2[4]
      } < /td> < /
      tr > <
      tr >
      <
      td > Friday < /td> <
      td > {
        this.state.average2[5]
      } < /td> < /
      tr > <
      tr >
      <
      td > Total Avg < /td> <
      td > {
        this.state.total
      } < /td> < /
      tr > <
      /tbody> < /
      table > < br / > < br / > < br / >
      <
      /div>
    );
  }

}

export default Table;